/**
 * Created by Dmitry Vereykin on 7/29/2015.
 */
import java.text.DecimalFormat;
import java.util.Scanner;

public class RecursivePopulation {
    static DecimalFormat df = new DecimalFormat("#,##0");

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int organisms;
        double percentIncrease;
        int days;
        int dayCount = 0;

        while (true) {
            System.out.print("Please Enter the starting number of organisms (at least 2): ");
            organisms = keyboard.nextInt();

            System.out.print("Please enter average daily % of population increase (positive): ");
            percentIncrease = keyboard.nextInt();

            System.out.print("Please enter the number of days (at least 1): ");
            days = keyboard.nextInt();

            if (organisms >= 2 && percentIncrease > 0 && days >= 1)
                break;
            else
                System.out.println("\nInvalid input data!\n");

        }

        System.out.println("\nDay " + (dayCount + 1) + "  :  " + organisms + " organisms.");
        population(organisms, percentIncrease, days, dayCount);
        keyboard.close();
    }

    public static double population(double organisms, double percentIncrease, int days, int dayCount){
        dayCount++;

        if (dayCount == days){
            return organisms;
        } else {
            organisms = (organisms + (organisms * (percentIncrease / 100)));
            System.out.println("Day " + (dayCount + 1) + "  :  " + df.format(organisms) + " organisms.");
            return population(organisms, percentIncrease, days, dayCount);
        }
    }
}